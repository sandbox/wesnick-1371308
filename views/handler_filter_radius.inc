<?php
// $Id$

/**
 * Filter radius
 */
class handler_filter_radius extends views_handler_filter {
  // This is always single, because of the distance field's possible dependency
  // on it.
  var $no_single = TRUE;

  function option_definition() {
    $options = parent::option_definition();

    $options['identifier'] = array('default' => 'dist');

    $options['value'] = array(
      'locpick' => array(
       'latitude' => 0,
       'longitude' => 0,
      ),
      'distance' => array(
        'search_distance' => 40,
      ),
    );

    $options['expose']['contains']['gmap_macro'] = array('default' => '[gmap ]');

    return $options;
  }

  function admin_summary() {
    return '';
  }

  /**
   * Shortcut to display the value form.
   */
  function show_value_form(&$form, &$form_state) {
    $this->value_form($form, $form_state);
    if (empty($this->no_operator)) {
      $form['value']['#prefix'] = '<div class="views-left-70">' . (isset($form['value']['#prefix']) ? $form['value']['#prefix'] : '');
      $form['value']['#suffix'] = (isset($form['value']['#suffix']) ? $form['value']['#suffix'] : '') . '</div>';
    }
  }

  function value_form(&$form, &$form_state) {
    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
    }

    $view = $this->view;
    if (isset($view->exposed_input[$identifier])) {
      $exposed_input = $view->exposed_input[$identifier];
    }
    else {
      $exposed_input = $this->value;
    }

    $form['value'] = array(
      '#tree' => TRUE,
    );

    $form['#attributes'] = array('class' => array('search-api-location'));

    $form['value']['locpick'] = array(
      '#type' => 'container',
      '#weight' => 0,
    );
    $form['value']['locpick']['latitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#default_value' => $exposed_input['locpick']['latitude'],
      '#weight' => 0,
    );
    $form['value']['locpick']['longitude'] = array(
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#default_value' => $exposed_input['locpick']['longitude'],
      '#weight' => 5,
    );

    $form['value']['distance'] = array(
      '#type' => 'container',
      '#weight' => 10,
    );

    $form['value']['distance']['search_distance'] = array(
      '#type' => 'textfield',
      '#title' => t('Distance, km:'),
      '#default_value' => $exposed_input['distance']['search_distance'],
    );

  }

  /**
   * Validate the options form.
   */
  function value_validate($form, &$form_state) {
    if (isset($form_state['values']['options']['value'])) {
      $values = $form_state['values']['options']['value'];
      if (!is_numeric($values['locpick']['latitude'])) {
        form_set_error('latitude', t('Latitude value must be not empty and numeric'));
      }
      if (!is_numeric($values['locpick']['longitude'])) {
        form_set_error('longitude', t('Longitude value must be not empty and numeric'));
      }
      if (!is_numeric($values['distance']['search_distance']) || $values['distance']['search_distance'] < 0.25) {
        form_set_error('search_distance', t('Distance value must be not empty and greater then 0.25'));
      }
    }
  }

  function exposed_form(&$form, &$form_state) {
    parent::exposed_form($form, $form_state);

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
    }

    $view = $this->view;
    if (isset($view->exposed_input[$identifier])) {
      $exposed_input = $view->exposed_input[$identifier];
    }
    else {
      $exposed_input = $this->value;
    }

    drupal_add_library('system', 'ui.slider');
    drupal_add_js(drupal_get_path('module', 'search_api_location') . '/js/search_api_location.js');

    $form[$identifier]['distance']['search_distance'] += array(
      '#id' => 'search_distance_value',
      '#size' => '10',
      '#weight' => 4,
    );

    $form[$identifier]['distance']['search_distance_slider'] = array(
      '#type' => 'item',
      '#weight' => 5,
      '#suffix' => '<div id="search_distance_slider"></div>',
    );
  }

  function exposed_validate($form, &$form_state) {
    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (isset($form_state['values'][$identifier])) {
        $values = $form_state['values'][$identifier];
        if (!is_numeric($values['locpick']['latitude']) || !is_numeric($values['locpick']['longitude'])) {
          form_error($form, t('You need to choose search position.'));
        }
      }
    }
  }

  function query() {
    if (empty($this->value)) {
      return;
    }elseif (!isset($this->value[0]['locpick']['latitude']) || empty($this->value[0]['locpick']['latitude']) || !isset($this->value[0]['locpick']['longitude']) || empty($this->value[0]['locpick']['longitude']) || !isset($this->value[0]['distance']['search_distance']) || empty($this->value[0]['distance']['search_distance'])) {
      return;
    }

    $index = search_api_index_load(substr($this->table, 17));
    if (!isset($index) || !is_object($index) || !$index->enabled) {
      return;
    }

    $options = array_merge($this->options, $this->options['value'], $this->value[0]);

    //convering to miles (because localsolr search use only miles)
    $radius = $options['distance']['search_distance']*0.621371192237;

    $search_options = array(
      'query_type' => 'geo',
      'lat' => $options['locpick']['latitude'],
      'lng' => $options['locpick']['longitude'],
      'radius' => $radius,
      'sort' => 'asc',
    );
    $query = $this->query->getSearchApiQuery();
    foreach ($search_options as $key => $value) {
      $query->setOption($key, $value);
    }

    $old = $this->query->getOriginalKeys();
    $keys = &$this->query->getKeys();
    if ($old) {
      $keys = &$this->query->getKeys();
      if (is_array($keys)) {
        $keys[] = $old;
      }
      elseif (is_array($old)) {
        // We don't support such nonsense.
      }
      else {
        $keys = "($old) ($keys)";
      }
    }
  }
}
