<?php
/**
 * @file
 * Provide views data for Search API Location
 */


/**
 * Implements hook_views_data().
 */
function search_api_location_views_data() {
  $data = array();
  foreach (search_api_index_load_multiple(FALSE) as $index) {
    $data['search_api_index_' . $index->machine_name]['radius'] = array(
      'group' => t('Search'),
      'title' => t('Radius'), // The item it appears as on the UI,
      'help' => t('Location radius for @name index', array('@name' => $index->name)), // The help that appears on the UI,
      'filter' => array(
        'handler' => 'handler_filter_radius',
        'label' => t('Radius'),
      ),
    );
  }
  return $data;
}


/**
 * Implements hook_views_data_alter().
 */
function search_api_location_views_data_alter(&$data) {

  foreach (search_api_index_load_multiple(FALSE) as $index) {

    $key = 'search_api_index_' . $index->machine_name;

    $fields = search_api_location_get_spatial_fields($index->id);

    if ($fields) {

      $id = key($fields);
      $id = _entity_views_field_identifier($id, $data[$key]);

      $data[$key][$id]['title'] = "Spatial search on this index's Spatial Field";

      $data[$key][$id]['filter']['handler'] = 'handler_filter_spatial';
      $data[$key][$id]['type'] = 'latlng';
    }
  }
}

/**
 * Implementation of hook_views_handlers().
 */
function search_api_location_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'search_api_location') .'/views',
    ),
    'handlers' => array(
      'handler_filter_radius' => array(
        'parent' => 'views_handler_filter',
      ),
  		'handler_filter_spatial' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}


/**
 * Implements hook_views_plugins
 */
function search_api_location_views_plugins() {
  return array(
    'exposed_form' => array(
      'search_api_location' => array(
        'title' => t('Search API location'),
        'help' => t('Test plugin'),
        'handler' => 'search_api_location_plugin_exposed_form',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'parent',
      ),
      'search_api_location_map' => array(
        'title' => t('Search API Location Map'),
        'help' => t('Use a location map to select geographic search parameters'),
        'handler' => 'search_api_location_map_plugin_exposed_form',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'parent',
      ),
    ),
  );
}
