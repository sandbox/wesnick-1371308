<?php
// $Id$

/**
 * Filter radius
 */
class handler_filter_spatial extends SearchApiViewsHandlerFilter {

  // This is always multiple, because we can have distance, units etc.
  var $always_multiple = TRUE;


  function option_definition() {
    $options = parent::option_definition();

    $options['identifier'] = array('default' => 'map');
    $options['operator'] = array('default' => 'geofilt');
    $options['default_location'] = array('default' => 'callback');

    return $options;
  }


  /**
  * Extend the options form a bit.
  */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['default_location'] = array(
      '#type' => 'select',
      '#title' => t('Location Source Fallback'),
      '#options' => array(
        'callback' => t("Use search_api_location_default callback"),
        'defaults' => t("Use Search API Location processor defaults"),
      ),
      '#description' => t('Choose a method to determine values when none are provided in query parameters.'),
      '#default_value' => $this->options['location'] ? $this->options['location'] : 'callback',
    );

    $form['d_unit'] = array(
      '#type' => 'select',
      '#title' => t('Default Radius Unit'),
      '#options' => array(
        'm' => t('Meters'),
        'km' => t('Kilometers'),
        'miles' => t('Miles'),
      ),
      '#default_value' => $this->value['d_unit'],
      '#weight' => 10,
    );

    if (isset($form['expose'])) {
      $form['expose']['#weight'] = -5;
    }
  }

  function admin_summary() {
    return '';
  }

  function operator_options() {
    return array(
      'geofilt' => t('Radial Proximity'),
      'bbox' => t('Bounding Box Proximity'),
    );
  }


  function value_form(&$form, &$form_state) {

    $form['value'] = array(
    	'#markup' => t('Search API Location selector map is not available during live preview.'),
      '#weight' => 0,
    );

  }

  function exposed_form(&$form, &$form_state) {
    parent::exposed_form($form, $form_state);
    $key = $this->options['expose']['identifier'];
    $origin = $this->options['default_location'];

    $form['#tree'] = TRUE;
    // Bad things happen if we try to show a gmap in the views live preview.
    if (isset($form_state['input']['live_preview']) && $form_state['input']['live_preview']) {
      $form[$key] = array(
        '#markup' => t('Search API Location selector map is not available during live preview.'),
        '#weight' => 0,
      );
    }
    else {
      $form[$key] = array(
        '#type' => 'search_api_location_gmap',
        '#title' => 'Search Location',
        '#weight' => 0,
        '#settings'  =>array(
          'lat' => 40.738766565405946,
          'lng' => -73.99277687072754,
          'd' => exp(10),
        ),
      );
    }

    unset($form['default_location']);
    unset($form['d_unit']);
  }

  /**
  * Validate the options form.
  */
  function exposed_validate(&$form, &$form_state) {
  }


  function query() {

    if (empty($this->value)) {
      return;
    }

    if (isset($this->view->exposed_input[$this->field]) && isset($this->view->exposed_input[$this->field]['map'])) {
      $spatial = array();
      $spatial['sfield'] = $this->real_field;
      $spatial['pt'] = $this->view->exposed_input[$this->field]['map']['lat'] . ',' . $this->view->exposed_input[$this->field]['map']['lng'];
      $spatial['fq'] = '{!geofilt}';
      $spatial['d'] = exp($this->view->exposed_input[$this->field]['map']['d']) / 1000;
      $this->query->setOption('search_api_location', $spatial);

    }

  }


  /**
  * Add this filter to the query.
  */
  public function otheruery() {
    while (is_array($this->value)) {
      $this->value = $this->value ? reset($this->value) : NULL;
    }
    if ($this->value) {
      $this->query->condition($this->real_field, $this->value, $this->operator, $this->options['group']);
    }
  }

}