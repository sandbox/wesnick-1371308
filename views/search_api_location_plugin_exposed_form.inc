<?php
// $Id$

/**
 * The search_api_location plugin to handle radius exposed filter forms.
 */
class search_api_location_plugin_exposed_form extends views_plugin_exposed_form {


  function summary_title() {
    return t('Search API Location');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['use_gmap'] = array('default' => TRUE, 'bool' => TRUE);
    $options['gmap_macro'] = array('default' => '[gmap ]', 'translatable' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['use_gmap'] = array (
      '#type' => 'checkbox',
      '#title' => t('Use gmap'),
      '#description' => t('Use gmap to pick location with radius filter'),
      '#default_value' => $this->options['use_gmap'],
    );

    $form['gmap_macro'] = array(
     '#type' => 'textfield',
      '#title' => t('Gmap macro'),
      '#description' => t('The macro to use for the gmap. If you not specify gmap macro it will use default gmap settings.'),
      '#default_value' => $this->options['gmap_macro'],
      '#required' => TRUE,
      '#dependency' => array(
        'edit-exposed-form-options-use-gmap' => array(1)
      ),
      '#process' => array('ctools_dependent_process'),
    );
  }

  function exposed_form_alter(&$form, &$form_state) {
    parent::exposed_form_alter($form, $form_state);

    $view = $this->view;
    $filter = $view->filter['radius'];
    $identifier = $filter->options['expose']['identifier'];

    if (isset($view->exposed_input[$identifier]) && !empty($view->exposed_input[$identifier]['locpick']['latitude']) && !empty($view->exposed_input[$identifier]['locpick']['latitude'])) {
      $exposed_input = $view->exposed_input[$identifier];
    }
    elseif (isset($filter) && is_object($filter)) {
      $exposed_input = $filter->value;
    }
    else {
      return;
    }

    if (module_exists('gmap') && $this->options['use_gmap'] && isset($exposed_input) && isset($identifier)) {
      // Bad things happen if we try to show a gmap in the views live preview.
      if (isset($form_state['post']['live_preview'])) {
        $form['gmap']['proximity_map'] = array(
          '#value' => t('Gmap location selection is not available during live preview.'),
          '#weight' => 0,
        );
      }
      else {
        $map = array_merge(gmap_defaults(), gmap_parse_macro($this->options['gmap_macro']));
        $map['points'] = array();
        $map['pointsOverlays'] = array();
        $map['lines'] = array();

        $map['behavior']['locpick'] = TRUE;
        $map['behavior']['collapsehack'] = TRUE;

        $map['latitude'] = (float)$exposed_input['locpick']['latitude'];
        $map['longitude'] = (float)$exposed_input['locpick']['longitude'];
        $map['markers'][] = array(
          'latitude' => (float)$exposed_input['locpick']['latitude'],
          'longitude' => (float)$exposed_input['locpick']['longitude'],
          'text' => '<div class="gmap-popup">' . t('Your current search position') . '</div>',
          'markername' => 'treasure',
          'offset' => 0,
          'opts' => array(
            'clickable' => TRUE,
          ),
        );

        $map["shapes"][] = array (
          "type" => "circle",
          "style" => array("000000","1","50","0000ff","25"),
          "radius" => (float)$exposed_input['distance']['search_distance'],
          "center" => array((float)$exposed_input['locpick']['latitude'],(float)$exposed_input['locpick']['longitude']),
          'opts' => array(
            'clickable' => FALSE,
          ),
        );

        $mapWidth = str_replace('px', '', $map['width']);
        $mapHeight = str_replace('px', '', $map['height']);

        $map['zoom'] = floor(17-log (3300 * (float)$exposed_input['distance']['search_distance'] / min ($mapWidth, $mapHeight) ,2));

        //hide lockpick container
        $form[$identifier]['locpick']['#attributes'] = array('style' => array('display:none;'));

        $form[$identifier]['locpick']['latitude']['#map'] = $map['id'];
        gmap_widget_setup($form[$identifier]['locpick']['latitude'], 'locpick_latitude');

        $form[$identifier]['locpick']['longitude']['#map'] = $map['id'];
        gmap_widget_setup($form[$identifier]['locpick']['longitude'], 'locpick_longitude');

        $form[$identifier]['current_map'] = array('#type' => 'value', '#value' => $map);
       $form['current_mapid'] = array('#type' => 'hidden', '#value' => $map['id']);
        $form[$identifier]['gmap']['proximity_map']['locpick']['map'] = array(
          '#type' => 'gmap',
          '#weight' => -1,
          '#gmap_settings' => $map,
        );

        //part for no JS
        $form[$identifier]['address'] = array(
          '#type' => 'textfield',
          '#title' => t('Address:'),
          '#description' => t('Enter search address.'),
          '#prefix' => '<div id="search_address_field">',
          '#suffix' => '</div>',
          '#default' => (isset($exposed_input['address']))?$exposed_input['address']:'',
          '#weight' => 3,
        );

      }
    }
  }

  function exposed_form_submit(&$form, &$form_state, &$exclude) {
    $view = $this->view;
    $filter = $view->filter['radius'];
    $identifier = $filter->options['expose']['identifier'];

    $values = $form_state['values'];
    $gmap_key = variable_get('googlemap_api_key', '');
    if ($values[$identifier]['address'] && !empty($gmap_key)) {
      $url = url('http://maps.google.com/maps/geo', array('query' => array('q' => $values[$identifier]['address'], 'key' => $gmap_key)));
      $request = drupal_http_request($url);
      if ($request->code == '200') {
        $data = drupal_json_decode($request->data);
        if ($data['Status']['code'] == '200' && !empty($data['Placemark'])) {
          $form_state['view']->exposed_data[$identifier]['locpick']['longitude'] = $data['Placemark'][0]['Point']['coordinates'][0];
          $form_state['view']->exposed_data[$identifier]['locpick']['latitude'] = $data['Placemark'][0]['Point']['coordinates'][1];
        }
      }
    }
  }

  function pre_render($values) {
    $view = $this->view;
    if (isset($view->filter['radius'])) {
      $filter = $view->filter['radius'];
      if ($filter->is_exposed()) {
        $identifier = $filter->options['expose']['identifier'];
        if (isset($view->exposed_data[$identifier])) {
          $exposed_data = $view->exposed_data[$identifier];
        }
      }
    }

    if (!isset($exposed_data['current_map'])) {
      return;
    }

    foreach ($values as $marker) {
      $marker = (array) $marker;
      if (isset($marker['location:latitude']) && isset($marker['location:longitude']) && isset($marker['title']))
      $exposed_data['current_map']['markers'][] = array(
          'latitude' => $marker['location:latitude'],
          'longitude' => $marker['location:longitude'],
          'text' => '<div class="gmap-popup">' . $marker['title'] . '</div>',
          'markername' => 'drupal',
          'offset' => 0,
          'opts' => array(
            'clickable' => TRUE,
          ),
      );
    }

    $mapdefaults = gmap_defaults();
    $map = array_merge($mapdefaults, $exposed_data['current_map']);

    // Styles is a subarray.
    if (isset($exposed_data['current_map']['styles'])) {
      $map['styles'] = array_merge($mapdefaults['styles'], $exposed_data['current_map']['styles']);
    }
    gmap_map_cleanup($map);

    gmap_module_invoke('pre_theme_map', $map);

    unset($map["shapes"]);

    drupal_add_js(array('gmap' => array($exposed_data['current_map']['id'] => $map)), 'setting');
  }
}
