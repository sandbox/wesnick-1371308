<?php

/*
 * @file Search API Location hook documentation.
 */

/**
 *
 * Hook to alter search location parameters.
 *
 * @param string $keys
 *   The search keys
 * @param float $lat
 *   The input latitude or default latitude
 * @param float $lng
 *   The input longitude or default longitude
 * @param float $d
 *   Radial distance in meters
 */
function hook_search_api_location_default_position($keys, &$lat, &$lng, &$d) {

  // Ensure a minimum radius of 150m.
  if ($d <= 150) {
    $d = 150;
  }
}
