/**
 * Attaches the time slider behavior to the time select fields
 */
(function ($) {
  
  Drupal.searchAPILocationMap = {};  
  Drupal.searchAPILocationMap.maps = [];
  Drupal.searchAPILocationMap.markers = [];
  Drupal.searchAPILocationMap.resultMarkers = [];
  Drupal.searchAPILocationMap.shapes = [];
  
  Drupal.searchAPILocationMap.resultsProcessed = false;
  
     
  Drupal.behaviors.searchAPILocationMap = {    
           
    attach: function (context, settings) {
      var set = settings.searchAPILocationMap;
      
      // Radius Slider      
      $( "#" + set.distanceSlider.slider_id , context).slider({
        min: set.distanceSlider.min,
        max: set.distanceSlider.max,
        step: set.distanceSlider.step,
        value: $('#' + set.distanceSlider.value_id).val(),
        slide: function( event, ui ) {
          $('#' + set.distanceSlider.value_id).val(ui.value);
            Drupal.searchAPILocationMap.setDistanceText();
        },
        change: function( event, ui ) {
          $('#' + set.distanceSlider.value_id).val(ui.value);
        }
      })
      .css({'width' : $('#search-api-location-d-value').width() + 'px'});   
      
      
      // UNIT CHANGE
      
      $("#search-api-location-d-unit", context).bind('change', function(e) {
        Drupal.searchAPILocationMap.setDistanceText();
        $('.form-item-d-unit label:first').text($('#search-api-location-d-unit:checked').next('label').text());
      });
      
      
      // ADDRESS SEARCH
      
      $("#search-api-location-find", context).bind('click', function(e) {
        Drupal.searchAPILocationMap.geocodeAddress();
        return false;
      }); 
      
      
      // MAP
      
      if (!$('#' + set.gmap.elem).hasClass("map-processed")) {
        $('#' + set.gmap.elem).addClass("map-processed");
        // Bind the slidechange event to GMap Zoom
        $( "#" + set.distanceSlider.slider_id ).bind( "slidechange", function(event, ui) { 
          //var zoom 
          var zoom = Drupal.searchAPILocationMap.radiusCalc(ui.value);
          Drupal.searchAPILocationMap.setZoom(zoom);
          Drupal.searchAPILocationMap.updateRadiusShape();
        });
        
        var lat = $('#' + set.gmap.elem + '-lat').val();
        var lng = $('#' + set.gmap.elem + '-lng').val(); 
        
        set.gmap.map.radius = Drupal.searchAPILocationMap.getD('m');        
        Drupal.searchAPILocationMap.createMap(lat, lng, set.gmap.elem, set.gmap.map);
        Drupal.searchAPILocationMap.setDistanceText();
        Drupal.searchAPILocationMap.returnAddress();
      }

      if (!Drupal.searchAPILocationMap.resultsProcessed && settings.searchAPILocationMap && settings.searchAPILocationMap.gmap.point) {
        Drupal.ajax.prototype.commands.search_api_location_result_points(null, settings.searchAPILocationMap.gmap);
        
      }
      
    } 
  };
  

  Drupal.searchAPILocationMap.createMap = function(lat, lng, elem, settings) {
    // Create google encoded latlng
    var latLng = new google.maps.LatLng(lat, lng);

    var zoom = Drupal.searchAPILocationMap.radiusCalc(Drupal.searchAPILocationMap.getD('log'));
    // Set map options
    myOptions = {
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: true
    };
    
    myOptions = $.extend(myOptions, settings);
    
    myOptions.center = latLng;
    myOptions.zoom = zoom;
    // Create map
    Drupal.searchAPILocationMap.maps[0] = new google.maps.Map(document.getElementById(elem), myOptions);
        
    if (lat && lng) {
      // Set initial marker
      Drupal.searchAPILocationMap.setMapMarker(latLng);
    }
    
    // Listener to set marker
    google.maps.event.addListener(Drupal.searchAPILocationMap.maps[0], 'click', function(me){
      // Set a timeOut so that it doesn't execute if dbclick is detected
      singleClick = setTimeout(function(){   
        
        console.log(me);
        $("#search-api-location-map-lat").val(me.latLng.Qa);
        $("#search-api-location-map-lng").val(me.latLng.Ra);
        Drupal.searchAPILocationMap.setMapMarker(me.latLng);
        Drupal.searchAPILocationMap.returnAddress();
      }, 500);
    });

    // Detect double click to avoid setting marker
    google.maps.event.addListener(Drupal.searchAPILocationMap.maps[0], 'dblclick', function(me){
      clearTimeout(singleClick);
    });
    
  };
  
 
  Drupal.searchAPILocationMap.updateRadiusShape = function() {
    
    if(Drupal.searchAPILocationMap.shapes[0]) {
      Drupal.searchAPILocationMap.shapes[0].setMap(null);
    }
        
    var map = Drupal.searchAPILocationMap.maps[0];
    var latLng = Drupal.searchAPILocationMap.returnLatLng('gmap');

    
    var circleopts = {
        'center' : latLng,
        'clickable' : false,
        'fillColor' : '#444',
        'fillOpacity': 0.2,
        'strokeColor': '#444',
        'radius': Math.round(Drupal.searchAPILocationMap.getD('m')),        
    };
    
    Drupal.searchAPILocationMap.shapes[0] = new google.maps.Circle(circleopts);
    Drupal.searchAPILocationMap.shapes[0].setMap(map);
    map.panTo(latLng);
  };
  
   
  Drupal.searchAPILocationMap.updateLatLng = function(lat, lng, elem) {
    // Update the lat and lng input fields
    $('#' + elem + '-lat input').val(lat);
    $('#' + elem + '-lng input').val(lng);
  };
  
  Drupal.searchAPILocationMap.returnLatLng = function(format) {
    var lat = $("#search-api-location-map-lat").val();
    var lng = $("#search-api-location-map-lng").val();
    
    if (format == 'gmap') {
      return new google.maps.LatLng(lat, lng);
    }
    return [lat,lng];
  };
  
  
  
  Drupal.searchAPILocationMap.updateAddress = function(value) {
    $("#search-api-location-find-text").val(value);
  };
  
  
  Drupal.searchAPILocationMap.returnAddress = function() {
    var geocoder = new google.maps.Geocoder();
    var latLng = Drupal.searchAPILocationMap.returnLatLng('gmap');    
    // Update the address field
    if (geocoder) {
      geocoder.geocode({'latLng': latLng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          $('#search-api-location-find-text').val(results[0].formatted_address);
         }        
      });
    }    
  };
  
  Drupal.searchAPILocationMap.geocodeAddress = function() {

    var value = $("#search-api-location-find-text").val();
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': value }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        $("#search-api-location-map-lat").val(results[0].geometry.location.Pa);
        $("#search-api-location-map-lng").val(results[0].geometry.location.Qa);
        Drupal.searchAPILocationMap.setMapMarker(results[0].geometry.location);
        Drupal.searchAPILocationMap.updateAddress(results[0].formatted_address);
      }
    });
  };


   Drupal.searchAPILocationMap.setZoom = function(zoom) {
     
     if(Drupal.searchAPILocationMap.maps[0].getZoom() != zoom) {
       Drupal.searchAPILocationMap.maps[0].setZoom(zoom);
     }          
   };
   
   
   Drupal.searchAPILocationMap.setDistanceText = function() {
     var value = $('#search-api-location-radius-slider').slider("option", "value");
     value = Math.exp(value);
     var unit = $('#search-api-location-d-unit:checked').val();
     $('#search-api-location-d-value').val(Drupal.searchAPILocationMap.metersToValue(value, unit));     
   };
   
   Drupal.searchAPILocationMap.metersToValue = function(value, unit) {     
    
     var string = '';
     if (unit == 'm') {
       string = 'Meters';
            
     }
     else if (unit == 'miles') {
       string = 'Miles';
       value = value / 1609.344;
     }
     else if (unit == 'km') {
       string = 'KM';
       value = value / 1000;
     }    
     value = value.toFixed(2).toString() + ' ' + string;
     return value;
   };


   Drupal.searchAPILocationMap.radiusCalc = function(value) {   
     var ret = 20 - Math.ceil(value);
     return ret;     
   };
   
   Drupal.searchAPILocationMap.getD = function(format) {
     var value = $('#search-api-location-radius-slider').slider("option", "value");
     
     if (format == 'log') {
       return value;
     }
     
     value = Math.exp(value); 
     
     if (format == 'diameter') {
       value = value * 2 + (value * 0.2); //diameter with 10% padding on each side       
     } 
     
     return value;     
   };

   
  Drupal.searchAPILocationMap.setMapMarker = function(latLng) {
    // remove old marker
    if (Drupal.searchAPILocationMap.markers[0]) {
      Drupal.searchAPILocationMap.markers[0].setMap(null);
    }
    Drupal.searchAPILocationMap.markers[0] = new google.maps.Marker({
      map: Drupal.searchAPILocationMap.maps[0],
      draggable: false,
      position: latLng
    });
        
    Drupal.searchAPILocationMap.updateRadiusShape();
    
    
    return false; 
  };
 

  Drupal.ajax.prototype.commands.search_api_location_result_points = function(ajax, response, status) {
  
    for(var i in response.point) {
      var latLng = new google.maps.LatLng(response.point[i].lat, response.point[i].lng);
      if (Drupal.searchAPILocationMap.resultMarkers[i]) {
        Drupal.searchAPILocationMap.resultMarkers[i].setMap(null);
      }
      Drupal.searchAPILocationMap.resultMarkers[i] = new google.maps.Marker({
        map: Drupal.searchAPILocationMap.maps[0],
        draggable: false,
        animation: google.maps.Animation.DROP,
        position: latLng
      });      
    }
    
  };
 
  
})(jQuery);