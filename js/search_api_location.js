(function ($) {

//Anton part
        var A_step = 0.25;
        var A = 4;
        var B_step = 1;
        var B = 20;
        var C_step = 5;
        var C = 50;
        var D_step = 10;
        var D = 100;
        var E_step = 50;
        var E = 500;


        var numA = A / A_step;
        var numB = (B - A) / B_step + numA;
        var numC = (C - B) / C_step + numB;
        var numD = (D - C) / D_step + numC;
        var numE = (E - D) / E_step + numD;


//END of Anton part
        var mapname;

  $(document).ready(function(){
   $( "#search_distance_value" ).attr('readonly', true);
   $( "#search_distance_value" ).css('border', 0);
   mapname = $('[name=current_mapid]').val();
    if ($.isFunction(GBrowserIsCompatible)) {
      if (mapname && GBrowserIsCompatible()) {
        $( "#search_address_field" ).attr('enabled', false);
        $( "#search_address_field" ).hide();
        var obj = Drupal.gmap.getMap(mapname);
        if (!obj) {
          return;
        }
        obj.bind('ready', function(data) {
          var map = Drupal.gmap.getMap(mapname);
          GEvent.addListener(map.map, "click", function(overlay, point) {
            if (!overlay) {
              var locpick = new GLatLng(point.y, point.x);
              change_gmap_shapes(locpick, false);
              GEvent.addListener(map.locpick_point, 'dragend', function () {
                var locpick = map.locpick_point.getLatLng();
                change_gmap_shapes(locpick, false);
              });
            }
          });
          GEvent.addListener(map.locpick_point, 'dragend', function () {
            var locpick = map.locpick_point.getLatLng();
            change_gmap_shapes(locpick, false);
          });
        });
      }
    }
    $( "#search_distance_slider" ).slider({

      value: get_current_distance(),
      min: 1,
      max: numE,
      step: 1,
      slide: function( event, ui ) {
        var value = ui.value;
        var newValue;
//Anton part
        if (value >= 0.25 && value <= numA) {
          newValue = (value * A_step).toFixed(2);
        }
        if (value > numA && value <= numB) {
          newValue = (value - numA) * B_step + A;
        }
        if (value > numB && value <= numC) {
          newValue = (value - numB) * C_step + B;
        }
        if (value > numC && value <= numD) {
          newValue = (value - numC)* D_step + C;
        }
        if (value > numD && value <= numE) {
          newValue = (value - numD)* E_step + D;
        }
//END of Anton part
        if (mapname) {
          var map = Drupal.gmap.getMap(mapname);
          if (map) {
            change_gmap_shapes(map.locpick_coord, newValue);
          }
        }

        $( "#search_distance_value" ).val(newValue);
      }
    });
  });


  //HELPER functions



 /*
  * Workaround radius shape (move it to new lockation or change radius)
  */
  function change_gmap_shapes (locpick, radius) {
    var obj = Drupal.gmap.getMap(mapname);
    var new_shapes = new Array();
    if (obj.vars.shapes) {
      jQuery.each(obj.vars.shapes, function (i, n) {
        obj.map.removeOverlay(n.shape);

        // New shape
        var pa, cargs, style;
        pa = [];

        if (radius) {
          n.radius = radius;
        }

        pa = obj.poly.calcPolyPoints(locpick, n.radius * 1000, n.numpoints);
        cargs = [pa];
        style = obj.vars.styles.poly_default.slice();
        style = n.style.slice();
        style[0] = '#' + style[0];
        style[1] = Number(style[1]);
        style[2] = style[2] / 100;
        style[3] = '#' + style[3];
        style[4] = style[4] / 100;

        jQuery.each(style, function (i, l) {
          cargs.push(l);
        });
        if (n.opts) {
          cargs.push(n.opts);
        }
        var Pg = function (args) {
          GPolygon.apply(this, args);
        };
        Pg.prototype = new GPolygon();
        var Pl = function (args) {
          GPolyline.apply(this, args);
        };
        Pl.prototype = new GPolyline();
        n.shape = new Pg(cargs);
        obj.map.addOverlay(n.shape);
        new_shapes[i] = n;
      });
      obj.vars.shapes = new_shapes;
    }
  }

/*
 * Calculate value position on slider
 */
  function get_current_distance() {
    var value = $( "#search_distance_value" ).val();
    if (value == 'undefined') {
      value = 0.25;
    }
    var newValue;

    if (value >= 0.25 && value <= A) {
      newValue = (value / A_step).toFixed(2);
    }
    if (value > A && value <= B) {
      newValue = (value - A) / B_step + numA;
    }
    if (value > B && value <= C) {
      newValue = (value - B) / C_step + numB;
    }
    if (value > C && value <= D) {
      newValue = (value - C) / D_step + numC;
    }
    if (value > D && value <= E) {
      newValue = (value - D) / E_step + numD;
    }

    return newValue;
  }
})(jQuery);