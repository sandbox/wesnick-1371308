<?php

/*
 * @file Search API Location theme file
 */

/**
 * Theme function for displaying search results.
 *
 * @param array $variables
 *   An associative array containing:
 *   - index: The index this search was executed on.
 *   - result: One item of the search results, an array containing the keys
 *     'id' and 'score'.
 *   - item: The loaded item corresponding to the result.
 *   - keys: The keywords of the executed search.
 */
function theme_search_api_location_result(array $variables) {
  $index = $variables['index'];
  $id = $variables['result']['id'];
  $item = $variables['item'];

  $wrapper = $index->entityWrapper($item, FALSE);

  $url = $index->datasource()->getItemUrl($item);
  $name = $index->datasource()->getItemLabel($item);

  $fields = $index->options['fields'];

  // Process Spatial Fields
  $variables['points'] = array();
  foreach($fields as $fname => $field) {
    if (isset($field['real_type']) && $field['real_type'] == 'latlng') {
      if (strpos($fname, ':') === FALSE) {
        $tree = $fname;
      }
      else {
        $tree = explode(':', $fname);
      }
      switch (count($tree)) {
        case 1:
          if (!is_null($wrapper->{$tree}->value())) {
            $latlng = explode(',', $wrapper->{$tree}->value());
          }

          break;
        case 2:
          list($a, $b) = $tree;
          if (!is_null($wrapper->{$a}->value())) {
            $latlng = explode(',', $wrapper->{$a}->{$b}->value());
          }
          break;
        case 3:
          list($a, $b, $c) = $tree;
          if (!is_null($wrapper->{$a}->value())) {
            $latlng = explode(',', $wrapper->{$a}->{$b}->{$c}->value());
          }
          break;
      }
      if (isset($latlng) && count($latlng)) {

        $variables['points'][$id] = search_api_command_add_result($latlng[0], $latlng[1]);
      }
      continue;
    }
  }

  if ($variables['points']) {

    drupal_add_js(array('searchAPILocationMap' => array('gmap' => array('point' => $variables['points']))), 'setting');
  }


  $fields = array_intersect_key($fields, drupal_map_assoc($index->getFulltextFields()));
  $fields = search_api_extract_fields($wrapper, $fields);
  $text = '';
  $length = 0;
  foreach ($fields as $field) {
    if (search_api_is_list_type($field['type']) || !isset($field['value'])) {
      continue;
    }
    $val_length = drupal_strlen($field['value']);
    if ($val_length > $length) {
      $text = $field['value'];
      $length = $val_length;
    }
  }
  if ($text && function_exists('text_summary')) {
    $text = text_summary($text);
  }


  $output = '<h3>' . ($url ? l($name, $url['path'], $url['options']) : check_plain($name)) . "</h3>\n";
  if ($text) {
    $output .= $text;
  }

  return $output;
}
