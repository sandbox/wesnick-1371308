<?php




class spatialProcessor extends SearchApiAbstractProcessor {

  /**
   * Constructor
   */
  public function __construct(SearchApiIndex $index, array $options = array()) {
    parent::__construct($index, $options);
    $this->options += array(
      'geo_searchtype' => 1,
      'geo_position_callback' => '',
      'geo_default_lat' => 29.68,
      'geo_default_lng' => -82.27,
      'geo_default_radius' => 5,
      'geo_default_use_keys' => TRUE,
    );
  }

  public function supportsIndex(SearchApiIndex $index) {

    foreach ($index->options['fields'] as $key => $value) {
      if (isset($value['real_type']) && in_array($value['real_type'], array('latlng', 'geohash'))) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function configurationForm() {
    $form = parent::configurationForm();

    foreach ($form['fields']['#options'] as $key => $value) {

      if (!isset($this->index->options['fields'][$key]['real_type']) || !in_array($this->index->options['fields'][$key]['real_type'], array('latlng', 'geohash'))) {
        unset($form['fields']['#options'][$key]);
        unset($form['fields']['#default_value'][$key]);
      }
    }

    $form['geo_searchtype'] = array(
      '#type' => 'radios',
      '#title' => t('Default Geo Search Type'),
      '#description' => t('Select the default type of spatial query you want to use.'),
      '#options' => array(
        1 => 'Distance Filter - Filter Results by Radial Distance (expermiental)',
        2 => 'Bounding Box Filter - Filter Results by Bounding Box (expermiental)',
        3 => 'Geo Distance - Add distance to Results Score (experimental)',
        4 => 'Geo Boost - Adds distance result as boost (expermiental)'
      ),
      '#default_value' => $this->options['geo_searchtype'],
    );

    $form['geo_default_explanation'] = array(
      '#markup' => '<h3>Select the default method to extract required spatial information.  The order of precedence
                    from most to least will be:</h3>
                    <ul>
                    <li>Search Keys, lat, lng, radius -- if they exist and "Use Search Keys" is checked</li>
                    <li>Callback function -- if one exists</li>
                    <li>Defaults entered below</li>
                    </ul>'
    );

    $form['geo_default_lat'] = array(
      '#title' => t('Default Latitude'),
      '#type' => 'textfield',
      '#default_value' => $this->options['geo_default_lat'],
      '#required' => TRUE,
    );

    $form['geo_default_lng'] = array(
      '#title' => t('Default Longitude'),
      '#type' => 'textfield',
      '#default_value' => $this->options['geo_default_lng'],
      '#required' => TRUE,
    );

    $form['geo_default_radius'] = array(
      '#title' => t('Default Radius (km)'),
      '#type' => 'textfield',
      '#default_value' => $this->options['geo_default_radius'],
      '#required' => TRUE,
    );

    $callback_options = array();
    foreach(module_implements('search_api_location_default_position') as $mod) {
      $function = $mod . '_search_api_location_default_position';
      if (function_exists($function)) {
        $callback_options[$function] = $mod . ' Position Callback';
      }
    }
    if (!empty($callback_options)) {
      $form['geo_position_callback'] = array(
        '#type' => 'select',
        '#title' => t('Default Geo Postion Callback Function'),
        '#description' => t('Select a default position callback function.'),
        '#options' => $callback_options,
        '#default_value' => $this->options['geo_position_callback'],
      );
    }

    $form['geo_default_use_keys'] = array(
      '#title' => t('Use Search Keys for Defaults (if available)'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['geo_default_use_keys'],
    );


    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    parent::configurationFormValidate($form, $values, $form_state);

    $fields = array_filter($values['fields']);
    if (empty($fields)) {
      $el = $form['fields'];
      form_error($el, $el['#title'] . ': ' . t('You must select suitable fields to index data with the LatLng processor.'));
    }
    if (count($fields) != 1 || strpos(key($fields), 'latlng') === FALSE) {
      $el = $form['fields'];
      form_error($el, $el['#title'] . ': ' . t('For the LatLng Datatype you must select LatLong Pair from a compatible Field.'));
    }
  }


  /**
   * Calls processKeys() for the keys and processFilters() for the filters.
   */
  public function preprocessSearchQuery(SearchApiQuery $query) {

    $keys = &$query->getKeys();
    $field = key($this->options['fields']);
    $sp = $_GET['spatial'];

    $lat = isset($sp['lat']) ? $sp['lat'] : $this->options['geo_default_lat'];
    $lng = isset($sp['lng']) ? $sp['lng'] : $this->options['geo_default_lng'];
    $d = isset($sp['d']) ? $sp['d'] / 1000 : $this->options['geo_default_radius'];


    if ($function = $this->options['geo_position_callback']) {
      $function($keys, $lat, $lng, $d);
    }

    $spatial = array();
    $spatial['sfield'] = $field;
    $spatial['pt'] = $lat . ',' . $lng;


    switch($this->options['geo_searchtype']) {
      case 1:
        // Radial Distance Filter
        $spatial['fq'] = '{!geofilt}';
        $spatial['d'] = $d;
        $query->setOption('search_api_location', $spatial);

        break;
      case 2:
        // Bounding Box Filter
        $spatial['fq'] = '{!bbox}';
        $spatial['d'] = $d;
        $query->setOption('search_api_location', $spatial);

        break;
      case 3:
        // Distance Added to Score
        $spatial['query'] = '{!func}geodist()';
        $spatial['sort'] = 'geodist() asc';
        $query->setOption('search_api_location', $spatial);
        break;
      case 4:

        $spatial['fq'] = "{!geofilt}";
        $spatial['d'] = $d;
        // @TODO: allow this to be configurable
        $spatial['bf'] = 'recip(geodist(),2,200,20)';
        $query->setOption('search_api_location', $spatial);

        break;
    }


  }


  /**
   * Calculates distance from a point if option is set.
   * @TODO: this should be removed for SOLR 4.x
   */
  public function postprocessSearchResults(array &$response, SearchApiQuery $query) {

    //@TODO: calculate distance
    return;
  }

  /**
   * Method for preprocessing field data.
   *
   * Calls process() either for the whole text, or each token, depending on the
   * type. Also takes care of extracting list values and of fusing returned
   * tokens back into a one-dimensional array.
   */
  protected function processField(&$value, &$type) {
    if (!isset($value) || $value === '') {
      return;
    }

    $this->processFieldValue($value);
  }
  /**
   * @param $name
   *   The field's machine name.
   * @param array $field
   *   The field's information.
   *
   * @return
   *   TRUE, if the field should be processed.
   */
  protected function testField($name, array $field) {
    if (empty($this->options['fields'])) {
      return $this->testType($field['type']);
    }
    return !empty($this->options['fields'][$name]);
  }

  /**
   * @return
   *   TRUE, if the type should be processed.
   */
  protected function testType($type) {
    // We are only going to process fields specifically chosen to be indexed
    return FALSE;
  }

}
